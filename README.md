# network-ee

This repo includes an EE for multiple vendor network devices. This process is meant for one-off lab environemts using AAP or using Ansible-Navigator for testing purposes.

Collection              
------------------------ 
ansible.netcommon        
ansible.network            
ansible.utils              
arista.eos                 
arubanetworks.aos_switch   
arubanetworks.aoscx        
cisco.ios                  
cisco.iosxr                
cisco.nxos                 
           


## Pulling the EE to your AAP Controller or Private Hub online.

The network-ee can be pulled without credentials from:
~~~
registry.gitlab.com/tdubiel1/network-ee:latest
~~~

## Closed environements
This method is available to create the EE container locally when unable to pull files from gitlab.com to the AAP controller or hub.

### 1. Clone this repository to a device with internet access
~~~
git clone https://gitlab.com/tdubiel1/network-ee.git
cd network-ee
~~~

### 2. File
network-ee.tar

### 3. Convert the above file back to a container with podman
~~~
podman load -i network-ee.tar 
~~~

### 4. Verify the container works and includes the collections
~~~
podman run --rm network-ee:latest ansible-galaxy collection list
~~~
Example Output:
~~~
# /usr/share/ansible/collections/ansible_collections
Collection               Version
------------------------ -------
ansible.netcommon        3.1.1  
ansible.utils            2.6.1  
arista.eos               5.0.1  
arubanetworks.aos_switch 1.5.0  
arubanetworks.aoscx      4.1.0  
cisco.ios                3.3.2  
cisco.iosxr              3.3.1  
cisco.nxos               3.1.2  
clay584.genie            0.1.11 
community.general        5.6.0  
community.network        4.0.1   
~~~

### 5 EE to Contoller or Ansible-Navigator

Use steps above or othe copy method to get container image to the AAP Controller"
file:
~~~
network-ee.tar
~~~
~~~
podman load -i network-ee.tar 
~~~

AAP Controller:

* Execution Environments
   * Add
       * localhost/network-ee:latest
           * Never pull container before running
            

[for later]
Ansible-navigator:
Modify ansible-navigator.yml "image"
~~~
ansible-navigator:
  ansible:
    inventory:
      entries:
      - hosts
  execution-environment:
    container-engine: podman
    enabled: true
    image: network-ee:latest
    pull:
      policy: always
~~~

## Ansible Navigator Install
Ansible Navigator is a great Tool for testing the EE and for Playbook development

Install the python package manager using the system package installer (e.g.):

~~~
sudo dnf install python3-pip
~~~

Install ansible-navigator:
~~~
python3 -m pip install ansible-navigator --user
~~~
Add the installation path to the user shell initialization file (e.g.):
~~~
echo 'export PATH=$HOME/.local/bin:$PATH' >> ~/.profile
~~~
Refresh the PATH (e.g.):
~~~
source ~/.profile
~~~
Launch ansible-navigator:
~~~
ansible-navigator
~~~
ansible-navigator triggers a one-time download of the demo execution-environment image.


### EE verification with Ansible Navigator
Display the active EE image
~~~
ansible-navigator images
~~~
Display the Installed Collections
~~~
ansible-navigator collections
~~~
Display the installed Python Modules
~~~
ansible-navigator exec "pip list"
~~~
Run a Playbook
~~~
ansible-navigator run arista-show.yml -m stdout
~~~
